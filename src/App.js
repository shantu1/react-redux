import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import Navbar from './components/NavBar';
import Home from './routes/Home';
import Register from './routes/Register';


function App() {
  return (
    <Router>
      <CssBaseline />
      <div>
        <Navbar />
        {/* A <Switch> looks through its children <Route>s and
          renders the first one that matches the current URL. */}
        <Switch>
          <Route path="/">
            <Home />
          </Route>
          <Route path="/register">
            <Register />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
